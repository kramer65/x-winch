--This is the launcher script for X-Winch.  If aircraft is designated as a glider, it runs X-Winch.lua, located in the X-Winch folder,
--which should be in the FlyWithLua/scripts directory.

dataref("aircraft_is_glider", "sim/aircraft2/metadata/is_glider", "readonly")

if aircraft_is_glider == 1 or (PLANE_ICAO == "FALKE") then
	dofile(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/X-Winch.lua")
end
	