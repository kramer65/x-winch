--[[

X-Winch for X-Plane - Version: 1.0

Maurice Bent � Original Concept and Winch speed adjustment coding and Quality Control

Richard Every Clayton - Pop-Up menu concept and original coding

Joe Kipfer - IMGUI coding, Sound File editing, timer coding, object spawning, and generally making sure it all hangs together nicely � and works

Chris Evans - The dulcet tones of the launch controller, testing and QC

Stefan Schroen � Background environmental sounds, and the 3D artist behind the Skylaunch winch and all other objects and people.

]]

-----------SOUND FILES------------

hook_up_sound = load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/hookup sound effect raw.wav")
cable_break_sound = load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/cable_break.wav")
all_clear = load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/all_clear_muffled.wav")
all_clear_right = load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/all_clear_muffled_right.wav")
allOut = load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/allOut_muffled.wav")
allOut_right = load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/allOut_muffled_right.wav")
canopyAlert= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/canopy_open.wav")
airbrakeAlert= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/airbrake_alert.wav")
airbrakeAlert_right= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/airbrake_alert_right.wav")
cableOn= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/cableOn.wav")
cableOn_right= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/cableOn_right.wav")
enviroSound= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/enviro.wav")
enviroSoundMuffled= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/enviro_muffled.wav")
windNoise= load_WAV_file(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/windNoise.wav")

bgpic = float_wnd_load_image(SCRIPT_DIRECTORY .. "X-Winch/bgpic.png")
glidericon = float_wnd_load_image(SCRIPT_DIRECTORY .. "X-Winch/glidericon.png")




-----------DATAREFS------------

dataref("winchbhp", "sim/world/winch/winch_max_bhp", "writeable")
dataref("winch_cable_length", "sim/world/winch/winch_initial_length", "writeable")
dataref("airspeed", "sim/cockpit2/gauges/indicators/airspeed_kts_pilot", "readonly")
dataref("delay_timer", "sim/cockpit2/clock_timer/local_time_seconds", "writeable")
dataref("ftu_groundspeed","sim/flightmodel/position/groundspeed", "writeable") 
dataref("realFrame_time", "sim/operation/misc/frame_rate_period", "readonly") 
dataref("ftu_canopy_open", "sim/cockpit2/switches/canopy_open", "writeable") 
dataref("canopy_open_ratio", "sim/flightmodel2/misc/canopy_open_ratio", "readonly")
dataref("wing_roll","sim/flightmodel/position/phi", "writeable")
dataref("overRide","sim/operation/override/override_planepath", "writeable", 0)
dataref("Wind_WDir", "sim/cockpit2/gauges/indicators/wind_heading_deg_mag", "readonly")
dataref("Wind_WSpd", "sim/cockpit2/gauges/indicators/wind_speed_kts", "readonly")
dataref("SPEEDBRAKE", "sim/cockpit2/controls/speedbrake_ratio", "writeable") 
dataref("ftu_parking_brake_ratio", "sim/cockpit2/controls/parking_brake_ratio", "writeable") 
dataref("winchspeed", "sim/world/winch/winch_speed_knots", "writeable")
dataref("Yaero", "sim/flightmodel/forces/vy_air_on_acf", "writeable")
dataref("bus_rotation", "xwinch/bus_rotation", "writeable")
dataref("people_rotation", "xwinch/people_rotation", "writeable")
dataref("busOffset", "xwinch/bus_distance", "writeable")
dataref("peopleOffset", "xwinch/people_distance", "writeable")
dataref("chair1Offset", "xwinch/chair1_distance", "writeable")
dataref("chair1_rotation", "xwinch/chair1_rotation", "writeable")
dataref("chair2Offset", "xwinch/chair2_distance", "writeable")
dataref("chair2_rotation", "xwinch/chair2_rotation", "writeable")
dataref("AIRCRAFT_ALTITUDE_AGL", "sim/flightmodel/position/y_agl", "readonly")

dataref("chair1Switch", "xwinch/chair1_instanced", "writeable")
dataref("chair2Switch", "xwinch/chair2_instanced", "writeable")
dataref("peopleSwitch", "xwinch/people_instanced", "writeable")
dataref("busSwitch", "xwinch/bus_instanced", "writeable")
dataref("winchSwitch", "xwinch/winch_instanced", "writeable")
dataref("operatorSwitch", "xwinch/operator_instanced", "writeable")


-----------DIRECTLY SET VALUES------------

winchbhp = 0
winchspeed = 60




-----------FLAGS, VARIABLES, TIMERS, BOOLEANS------------


all_reset = 1
canopy_open_flag = 0
launch_status=0
take_up_slack=0
all_out=0
delay_time=0
hook_up_sound_flag = 0
cablebreak = 0
random_generated = 0
brakestoggled = false
soundtimer = 0
cable_attached = 0
Acceleration_multiplier = 4
canopy_open_sound_flag = 0
airbrake_alert_flag = 0
bhp_release = 0
hookuptimer = 0
canopy_sound_has_played = 0
airbrake_sound_has_played = 0 
readwinchlengthflag = 0
writewinchlengthflag = 1
winch_object_spawned = 0 
randomizer = 0
script_stop_timer = 0 
hold_brakes_until_allout = true
bus_rotation_adjusted = 0
people_rotation_adjusted = 0
chair1_rotation_adjusted = 0
chair2_rotation_adjusted = 0
winchlength_adjusted = 0
busOffset_adjusted = 0
peopleOffset_adjusted = 0
chair1Offset_adjusted = 0
chair2Offest_adjusted = 0
sliderVal = 0
sliderVal2 = 0
sliderVal3 = 0
sliderVal4 = 0
sliderVal5 = 0
sliderVal6 = 0
sliderVal7 = 0
sliderVal8 = 0
sliderVal9 = 0
spawnallbuttonreloader = 0
busreloader = 1 
peoplereloader = 1 
chair1reloader = 1 
chair2reloader = 1 


yellow = 0xFF00BFFF
light_blue = 0xFFF0E68C
light_green = 0xFF228B22
dark_green = 0xFF006400
bold_green = 0xFF00FF00
red = 0xFF0000FF
black = 0xFF000000
white = 0xFFFFFFFF





---------------------------------------------------------------
----------------------SOUND MIXER------------------------------
---------------------------------------------------------------


set_sound_gain(hook_up_sound,1)
set_sound_gain(cable_break_sound,1)
set_sound_gain(all_clear,.7)
set_sound_gain(allOut,.94)
set_sound_gain(canopyAlert,1)
set_sound_gain(airbrakeAlert,1)
let_sound_loop(enviroSound, true)





---------------------------------------------------------------
----------CANOPY CLOSER TIED TO NOT PRESENT BUTTON-------------
---------------------------------------------------------------


function canopy_not_present()
	ftu_canopy_open = 0
end
	




---------------------------------------------------------------
--------------WINCH LINE LENGTH PERSISTENCE READER-------------
---------------------------------------------------------------


function readwinchlength()
	
	if readwinchlengthflag == 0 then -- set to false by default, runs on first frame
	
	local winchlengthfile = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/winchlength.txt", "r")) -- open file as readable
	
	readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			winch_cable_length = readline -- set winch cable length to the number read on the first line
			logMsg("Winch Length Line set to last used length")
			if winch_cable_length == 0 then
				winch_cable_length = 1200
			end
		else
				winch_cable_length = 1200 -- if first line is nil, set to 1200
				logMsg("Winch Length line did not read successfully, set to default")
		end
		
	readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			bus_rotation = readline -- set winch cable length to the number read on the second line
			if bus_rotation == 0 then
				bus_rotation = 1.46
			end
		else
				bus_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busOffset = readline -- set winch cable length to the number read on the third line
			if busOffset == 0 then
				busOffset = 35
			end
		else
				busOffset = 35 -- if first line is nil, set to 1.46
		end
		
			readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			people_rotation = readline -- set winch cable length to the number read on the fourth line
			if people_rotation == 0 then
				people_rotation = 1.46
			end
		else
				people_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleOffset = readline -- set winch cable length to the number read on the fifth line
			if peopleOffset == 0 then
				peopleOffset = 30
			end
		else
				peopleOffset = 30 -- if first line is nil, set to 1.46
		end
		
					readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1_rotation = readline -- set winch cable length to the number read on the sixth line
			if chair1_rotation == 0 then
				chair1_rotation = 1.4
			end
		else
				chair1_rotation = 1.4 -- if first line is nil, set to 1.46
		end
		
		readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Offset = readline -- set winch cable length to the number read on the seventh line
			if chair1Offset == 0 then
				chair1Offset = 30
			end
		else
				chair1Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2_rotation = readline -- set winch cable length to the number read on the eighth line
			if chair2_rotation == 0 then
				chair2_rotation = 1.35
			end
		else
				chair2_rotation = 1.35 -- if first line is nil, set to 1.46
		end
		
		readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Offset = readline -- set winch cable length to the number read on the ninth line
			if chair2Offset == 0 then
				chair2Offset = 30
			end
		else
				chair2Offset = 30 -- if first line is nil, set to 1.46
		end
		
				readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busSwitch = readline -- 
		else
				busSwitch = 1 -- 
		end
		
						readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleSwitch = readline -- 
		else
				peopleSwitch = 1 -- 
		end
		
						readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Switch = readline -- 
		else
				chair1Switch = 1 -- 
		end
		
						readline = winchlengthfile:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Switch = readline -- 
		else
				chair2Switch = 1 -- 
		end
		
		
	winchlengthfile:close() -- can't forget to close after reading

	
	   readwinchlengthflag = 1
	
	
	end

end

---------------------------------------------------------------
---------------------DELAY START FOR SPAWN---------------------
---------------------------------------------------------------
delayonce = 0
how_long_2_wait = 0
function startdelay()
	if how_long_2_wait > 1  and hook_up_sound_flag == 0 and delayonce == 0 then -- hookup sound stops constant update
	overRide = 0
	command_once("x-winch/reload_winch_object")
	delayonce = 1   
	else
		how_long_2_wait = how_long_2_wait + realFrame_time
	end
end






---------------------------------------------------------------
--------------WINCH LINE LENGTH PERSISTENCE WRITER-------------
---------------------------------------------------------------

function writewinchlength()
	
	if writewinchlengthflag == 0 then -- so this only happens once, this flag is 1 by default, set to 0 on "hook up"
		
	local savefile = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/winchlength.txt", "w")) -- open file as writable
	local savesuccess = false
	
	savefile:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile:write (bus_rotation..'\n')
	savefile:write (busOffset..'\n')
	savefile:write (people_rotation..'\n')
	savefile:write (peopleOffset..'\n')
	savefile:write (chair1_rotation..'\n')
	savefile:write (chair1Offset..'\n')
	savefile:write (chair2_rotation..'\n')
	savefile:write (chair2Offset..'\n')
	savefile:write (busSwitch..'\n')
	savefile:write (peopleSwitch..'\n')
	savefile:write (chair1Switch..'\n')
	savefile:write (chair2Switch..'\n')
	
	
	
		if savefile then 
		savesuccess = true
		logMsg("Winch Length persistence saved successfully")
		else
		logMsg("Winch Length persistence did not save for some reason.")
		end
	
	savefile:close() -- can't forget to close the file after writing
	
	writewinchlengthflag = 1
	
	
	end
end
	
	
	
	
	
---------------------------------------------------------------
---------------CANOPY OPEN AND HOLD WINCH POWER AT 0-----------
---------------------------------------------------------------


function canopy_open_start() -- This sets the canopy to open on first frame
  if canopy_open_flag == 0 and AIRCRAFT_ALTITUDE_AGL < 5 then
    ftu_canopy_open = 1
	overRide = 1
	wing_roll = 7
    canopy_open_flag = 1 -- this is so the canopy isn't "held" open every frame.
  elseif canopy_open_flag == 0 and AIRCRAFT_ALTITUDE_AGL > 5 then
	overRide = 0
	canopy_open_flag = 1
  end
  if bhp_release == 0 then -- this is a flag called bhp_release.  when it == 1, it allows the winch to apply power.
    winchbhp = 0 -- this holds the winch at 0 power until bhp_release == 1
    winchspeed = 60
  end
  if winch_object_spawned == 0 and AIRCRAFT_ALTITUDE_AGL < 5 then
	command_once("x-winch/reload_winch_object")
  winch_object_spawned = 1
  end  
if hold_brakes_until_allout == true and AIRCRAFT_ALTITUDE_AGL < 5 then  ftu_parking_brake_ratio = 1 end
end






---------------------------------------------------------------
---------------MAIN RUNTIME CODE FOR WINCH OPS-----------------
---------------------------------------------------------------


function start_launch_sequence()

if all_reset == 1 then -- if we are in stoppage, reset all the timers to 0
	hookuptimer = 0
	delay_time = 0
	soundtimer = 0
end


if all_reset == 0 then -- This is the go/no-go flag for the entire sequence...if it == 1, nothing goes forward.  Set to 0 every time "Hook Up" command is re-initiated.  
	
math.randomseed(os.time()) -- allows "math random" function to throw out a different random number each time
						   -- without it, math random spits out the same number every time.

	if ftu_canopy_open == 1 and canopy_open_sound_flag == 0 and AIRCRAFT_ALTITUDE_AGL < 5 then
	play_sound(canopyAlert)
	logMsg("Canopy alert Played")
	canopy_open_sound_flag = 1 -- if sound has played, prevent it from playing again until flight restart on the winch
	canopy_sound_has_played = 1 -- to prevent speed up of timer (experimental)
	all_reset = 1 -- prevent the script from running further until start_launch_sequence() gets re-initiated.
	end

	if ftu_canopy_open == 0 and SPEEDBRAKE > 0 and airbrake_alert_flag == 0 and AIRCRAFT_ALTITUDE_AGL < 5 then
		if bus_rotation <= 3.14 then
		play_sound(airbrakeAlert)
		logMsg("Airbrake alert Played")
		airbrake_alert_flag = 1 -- if sound has played, prevent it from playing again until flight restart on the winch
		airbrake_sound_has_played = 1 -- to prevent speed up of timer (experimental)
		all_reset = 1 -- prevent the script from running further until start_launch_sequence() gets re-initiated.
		elseif bus_rotation > 3.14 then
		play_sound(airbrakeAlert_right)
		logMsg("Airbrake alert Played")
		airbrake_alert_flag = 1 -- if sound has played, prevent it from playing again until flight restart on the winch
		airbrake_sound_has_played = 1 -- to prevent speed up of timer (experimental)
		all_reset = 1 -- prevent the script from running further until start_launch_sequence() gets re-initiated.	
		end
	end

	if launch_status == 0 and ftu_canopy_open == 0 and SPEEDBRAKE <=0 and hook_up_sound_flag == 0 and AIRCRAFT_ALTITUDE_AGL < 5 then -- if all is correct then
	play_sound(hook_up_sound)
    writewinchlengthflag = 0 -- writes current scene to winchlength.txt, gets read and loaded every flight start
	logMsg("hookup sound played")
	canopy_open_sound_flag = 1 -- if sound never played, prevent it from playing after hookup (mid flight, flight end, etc)
	airbrake_alert_flag = 1 -- if sound never played, prevent it from playing after hookup
	hook_up_sound_flag = 1
	ftu_parking_brake_ratio = 1
	end

	if launch_status == 0 and hook_up_sound_flag == 1 then -- if hookup sound plays, count to 5 then play cableOn
	hookuptimer = hookuptimer + realFrame_time
	--logMsg(hookuptimer)  
		if hookuptimer > 4 then
			if bus_rotation <= 3.14 then
				play_sound(cableOn)
				launch_status = 1
			elseif bus_rotation > 3.14 then
				play_sound(cableOn_right)
				launch_status = 1
			end
		
		end
	end
  
	if launch_status == 1 and take_up_slack == 0 then -- if cableOn plays, count to 12 then play all_clear
	soundtimer = soundtimer + realFrame_time
   
   
-----------------------------------------------------------------------   
----------------------------Wing Leveler-------------------------------
-----------------------------------------------------------------------  

		
		if overRide < 1 then
			overRide = 1 -- give control to me, not x-plane physics
		end
		
		
		local pick_up_speed = 5 * realFrame_time -- this arithmatic keeps "rate" consistent no matter frame rate of user (number * realFrame_time dataref)
		local fast_up_speed = 6 * realFrame_time
		local step_down_one = 5 * realFrame_time
		local step_down_two = 4 * realFrame_time
		local settle_speed = 3 * realFrame_time
		if overRide == 1 then
			
			if wing_roll < -5 then
				wing_roll = wing_roll + pick_up_speed
				
			elseif wing_roll > -5 and wing_roll < -2.5 then
				wing_roll = wing_roll + fast_up_speed
				
			elseif wing_roll > -2.5 and wing_roll < -1.5 then
				wing_roll = wing_roll + step_down_one
				
			elseif wing_roll > -1.5 and wing_roll < -1.0 then
				wing_roll = wing_roll + step_down_two
				
			elseif wing_roll > -1.0 and wing_roll < -.07 then
				wing_roll = wing_roll + settle_speed
				
			elseif wing_roll > 5 then
				wing_roll = wing_roll - pick_up_speed
				
			elseif wing_roll < 5 and wing_roll > 2.5 then
				wing_roll = wing_roll - fast_up_speed
			
			elseif wing_roll < 2.5 and wing_roll > 1.5 then
				wing_roll = wing_roll - step_down_one
				
			elseif wing_roll < 1.5 and wing_roll > 1.0 then
				wing_roll = wing_roll - step_down_two
				
			elseif wing_roll < 1.0 and wing_roll > .07 then
				wing_roll = wing_roll - settle_speed
				
			end
		end
   
   
------------------------------------------------------------------------   
--------------------------All Clear All Around--------------------------
------------------------------------------------------------------------

   
		if soundtimer > 9 then
			if bus_rotation <= 3.14 then
			play_sound(all_clear)
			take_up_slack=1
			elseif bus_rotation > 3.14 then
			play_sound(all_clear_right)
			take_up_slack=1
			end
		end
	elseif launch_status == 1 and take_up_slack == 1 and all_out == 0 then -- if all_clear has played, count to 11 and set all_out flag to 1
	delay_time = delay_time + realFrame_time
	
		if delay_time > 8  then
			launch_status = 2
			take_up_slack = 0
			all_out = 1
		end
	
	elseif launch_status == 2 and take_up_slack == 0 and all_out == 1 then  -- if all out flag is 1, play allOut sound
		delay_time = 0 
		if bus_rotation <= 3.14 then
		play_sound(allOut)
		launch_status=3
		elseif bus_rotation > 3.14 then
		play_sound(allOut_right)
		launch_status=3
	    end
	elseif launch_status == 3 and take_up_slack == 0 and all_out == 1 then -- if all out has started, count to 2 then continue on to let winch have power
		delay_time = delay_time + realFrame_time 
		if delay_time > 2 then
		launch_status=4
		end
	end

	if launch_status==4 and take_up_slack == 0 and all_out == 1 then  -- finally, let winch have power after 2 seconds
	  bhp_release = 1 -- release "hold at 0 horsepower" logic, allow winch to apply power
	  script_stop_timer = script_stop_timer + realFrame_time -- timer to give brakes behavior back to aircraft
	
	
	--------------------------------
	----------BRAKE CONTROL---------
    --------------------------------

        if SPEEDBRAKE < 0.75
        then
            ftu_parking_brake_ratio = 0.0
			overRide = 0 -- give wings level info back to x-plane physics
        else
            ftu_parking_brake_ratio = (SPEEDBRAKE - 0.75) * 4.0 -- ground brake power at 0 through first 75% of throw
        end
        
		
	-----------------------------------------
	----------CABLE BREAK RANDOMIZER---------
    -----------------------------------------		
	
		
		if random_generated == 0 then
		cablebreak = math.random(1,20) -- this sets cablebreak to a random number between 1 and 20
		logMsg("cablebreak random number is ")
		logMsg(cablebreak)
		random_generated = 1
		end 
	
	
			
	-----------------------------------------
	----------WINCH POWER GOVERNOR-----------
    -----------------------------------------		
	
		
	

		if ftu_groundspeed < 5 then
			winchbhp=winchbhp + realFrame_time * (Acceleration_multiplier - 0.5)
			--command_once("sim/flight_controls/brakes_toggle_regular")
			
		end
		
		if ftu_groundspeed >= 5 and airspeed < 59 then
			winchbhp=winchbhp + realFrame_time * (Acceleration_multiplier+0.05)
		end
			--logMsg("starting power initiated")
			

		if airspeed > 58 and cablebreak > 18 then -- if random cablebreak number is greater than 18										 
			command_once("sim/flight_controls/winch_release") -- break the cable
			play_sound(cable_break_sound) -- play a "break" sound?
			reset_all() -- reset all conditions to default, resets all_reset to 1 preventing anything else from happening until re-initiation (see function below)
			logMsg("Cable is breaking")
		end
		
		if airspeed > 60 then
			winchbhp=winchbhp - (realFrame_time * Acceleration_multiplier)

			
		end
		
		if airspeed > 65 then
			winchbhp=winchbhp - realFrame_time * (Acceleration_multiplier +0.5) 
		end
		
		if script_stop_timer > 60 then
			logMsg("All Reset")
			all_reset = 1
			hold_brakes_until_allout = false
		end
	end
end
  

end




function initiate_winch_launch() -- This gets called when the command to initiate hookup is pressed.



   if AIRCRAFT_ALTITUDE_AGL < 5 then
	all_reset = 0
	local savefile = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/winchlength.txt", "w")) -- open file as writable
	local savesuccess = false
	
	savefile:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile:write (bus_rotation..'\n')
	savefile:write (busOffset..'\n')
	savefile:write (people_rotation..'\n')
	savefile:write (peopleOffset..'\n')
	savefile:write (chair1_rotation..'\n')
	savefile:write (chair1Offset..'\n')
	savefile:write (chair2_rotation..'\n')
	savefile:write (chair2Offset..'\n')
	savefile:write (busSwitch..'\n')
	savefile:write (peopleSwitch..'\n')
	savefile:write (chair1Switch..'\n')
	savefile:write (chair2Switch..'\n')
	
	
	
		if savefile then 
		savesuccess = true
		logMsg("Winch Length persistence saved successfully")
		else
		logMsg("Winch Length persistence did not save for some reason.")
		end
	
	savefile:close() -- can't forget to close the file after writing 
else
	all_reset = 1
   end   
end







function reset_all() -- disabling most of these as I don't think they are needed.  Further testing may prove me wrong.
all_reset=1
--canopy_open_sound_flag = 0
--airbrake_alert_flag = 0
--launch_status=0
--take_up_slack=0
--all_out=0
--delay_time=0
winch_cable_length=1200
winchspeed = 60
--winchbhp = 0
--hook_up_sound_flag = 0
--cablebreak = 0
end



-------------------------------------------------------
---------------------WIND NOISE------------------------
-------------------------------------------------------


windNoisePlayingquiet = false
windNoisePlaying = false
let_sound_loop(windNoise, true )
YaeroSync = Yaero

function windsounds()

--[[if Yaero > 0 then
set_sound_gain(windNoise, (Yaero / 3))	
end


if windNoisePlaying == false and Yaero > 0 then
let_sound_loop(windNoise, true )
play_sound (windNoise)
windNoisePlaying = true

set_sound_pitch(windNoise, 2.5)	

end]]
end


-------------------------------------------------------
----------------ENVIRONMENTAL SOUNDS-------------------
-------------------------------------------------------

soundplayingEnviro = false
soundplayingEnviroMuffled = false
let_sound_loop(enviroSound, true )
let_sound_loop(enviroSoundMuffled, true )
--let_sound_loop(enviroSoundMuffled, true )

function envirosounds()
	
	if ftu_groundspeed < 15 and canopy_open_ratio > .4 and soundplayingEnviro == false then
		
		set_sound_gain(enviroSound, .7)
		let_sound_loop(enviroSound, true)
		play_sound(enviroSound)
		stop_sound(enviroSoundMuffled)
		soundplayingEnviro = true
		soundplayingEnviroMuffled = false
		
	end
	
		
	if ftu_groundspeed < 15 and canopy_open_ratio < .2 and soundplayingEnviroMuffled == false then
		
		set_sound_gain(enviroSoundMuffled, .65)
		let_sound_loop(enviroSoundMuffled, true)
		play_sound(enviroSoundMuffled)
		stop_sound(enviroSound)
		soundplayingEnviroMuffled = true
		soundplayingEnviro = false
		
	end
	
	if ftu_groundspeed > 15 then
		stop_sound(enviroSound)
		stop_sound(enviroSoundMuffled)
		soundplayingEnviro = false
		soundplayingEnviroMuffled = false
	end
	
	
end
		




-----------------------------------------------------------------
-------------------------IMGUI SECTION---------------------------
-----------------------------------------------------------------


  general_tab = 1
  bus_tab = 0
  people_tab = 0
  chair1_tab = 0
  chair2_tab = 0  
function winchcp_on_build(lphp_wnd, x, y)

  local win_width = imgui.GetWindowWidth() -- for centering
  local win_height = imgui.GetWindowHeight() -- for centering
imgui.SetCursorPosX(80)
imgui.Image(bgpic, 300, 75) -- Header Logo Image

imgui.TextUnformatted("")

local cx10, cy10 = imgui.GetCursorScreenPos()

if general_tab == 1 then
--imgui.DrawList_AddRectFilled(cx10 + 5, cy10 -5, cx10 + 90, cy10 + 20, red, .5) -- Red Rectangle Behind Tabs
elseif bus_tab == 1 then
--imgui.DrawList_AddRectFilled(cx10 + 90, cy10 -5, cx10 + 175, cy10 + 20, red, .5)
elseif people_tab == 1 then
--imgui.DrawList_AddRectFilled(cx10 + 175, cy10 -5, cx10 + 267, cy10 + 20, red, .5)
--elseif chair1_tab == 1 then 
--imgui.DrawList_AddRectFilled(cx10 + 267, cy10 -5, cx10 + 351, cy10 + 20, red, .5)
elseif chair2_tab == 1 then 
--imgui.DrawList_AddRectFilled(cx10 + 351, cy10 -5, cx10 + 435, cy10 + 20, red, .5)
end

imgui.PushStyleColor(imgui.constant.Col.Button, light_blue)
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, yellow) 
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, dark_green) 
imgui.PushStyleColor(imgui.constant.Col.Text, black) 
imgui.SetCursorPosX(20)
if imgui.Button(" GENERAL ") then
	general_tab = 1
	bus_tab = 0
	people_tab = 0
    chair1_tab = 0
    chair2_tab = 0
	
end
 imgui.SameLine()
imgui.SetCursorPosX(105)
if imgui.Button("   BUS   ") then
	
	bus_tab = 1
	general_tab = 0
	people_tab = 0
    chair1_tab = 0
    chair2_tab = 0 
	 
end
 imgui.SameLine()
imgui.SetCursorPosX(190)
if imgui.Button("  PEOPLE  ") then
	people_tab = 1
	bus_tab = 0
	general_tab = 0
    chair1_tab = 0
    chair2_tab = 0
end
 imgui.SameLine()
imgui.SetCursorPosX(282)
if imgui.Button(" CHAIR 1 ") then
	bus_tab = 0
	general_tab = 0
	people_tab = 0
    chair1_tab = 1
    chair2_tab = 0
end
 imgui.SameLine()
imgui.SetCursorPosX(366)
if imgui.Button(" CHAIR 2 ") then
	bus_tab = 0
	general_tab = 0
	people_tab = 0
    chair1_tab = 0
    chair2_tab = 1
end

  imgui.PopStyleColor()   
  imgui.PopStyleColor()  
  imgui.PopStyleColor()   
  imgui.PopStyleColor() 

imgui.DrawList_AddLine(0, 124, 460, 124, 0xFF0000FF, 3) -- Red Line under Tabs


if general_tab == 1 then
bus_tab = 0
people_tab = 0
chair1_tab = 0
chair2_tab = 0  
	 imgui.TextUnformatted("")
	
	
-----------------------------------------------------------------
------------------------WINCH LENTGH SLIDER----------------------
-----------------------------------------------------------------	

	
	
	imgui.PushItemWidth(445) -- sets slider width to fixed value
	
	imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	local sliderVal = winch_cable_length
	
    imgui.SetCursorPosX(165)

	imgui.TextUnformatted ("Winch Cable Length:")
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
  local changed, newVal = imgui.SliderFloat("", sliderVal, 50, 2000, winch_cable_length - winch_cable_length % 1 .." Meters")
  if changed then
           sliderVal = newVal
			winch_cable_length = newVal
			command_once("x-winch/reload_winch_object")
  end	
  
  imgui.PopStyleColor()   
  imgui.PopStyleColor()  
  imgui.PopStyleColor()   
  imgui.PopStyleColor()  
  imgui.PopStyleColor()  
  imgui.PopStyleColor()  
  imgui.PopItemWidth()
 --[[imgui.SameLine()  -- possible future implementation, crashes sim
imgui.SetCursorPosX(280)
 if imgui.Button("Apply") then
	command_begin("sim/operation/Glider_Winch")
 end]]
 imgui.SetCursorPosX(90) 
 imgui.TextUnformatted("For the new cable length to take effect,")
 imgui.SetCursorPosX(90) 
 imgui.TextUnformatted("please 'hook up' then restart the flight."	 )
	 




-----------------------------------------------------------------
---------------------------FOUR BUTTONS--------------------------
-----------------------------------------------------------------



imgui.PushStyleColor(imgui.constant.Col.Button, bold_green) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_blue) --light blue
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, dark_green) --green
imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black


local cx, cy = imgui.GetCursorScreenPos()
 imgui.TextUnformatted("")
 imgui.TextUnformatted("") 
 imgui.SetCursorPosX(90) 
--imgui.PushStyleColor(imgui.constant.Col.ButtonActive, bold_green) --green 
--imgui.DrawList_AddRectFilled(cx + 75, cy + 29, cx + 145, cy + 58, dark_green, .5)
 if imgui.Button("HOOK UP") then
	local savefile = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/winchlength.txt", "w")) -- open file as writable
	local savesuccess = false
	
	savefile:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile:write (bus_rotation..'\n')
	savefile:write (busOffset..'\n')
	savefile:write (people_rotation..'\n')
	savefile:write (peopleOffset..'\n')
	savefile:write (chair1_rotation..'\n')
	savefile:write (chair1Offset..'\n')
	savefile:write (chair2_rotation..'\n')
	savefile:write (chair2Offset..'\n')
	savefile:write (busSwitch..'\n')
	savefile:write (peopleSwitch..'\n')
	savefile:write (chair1Switch..'\n')
	savefile:write (chair2Switch..'\n')
	
	
	
		if savefile then 
		savesuccess = true
		logMsg("Winch Length persistence saved successfully")
		else
		logMsg("Winch Length persistence did not save for some reason.")
		end
	
	savefile:close() -- can't forget to close the file after writing 
   if AIRCRAFT_ALTITUDE_AGL < 5 then
	all_reset = 0
else
	all_reset = 1
   end   

 end
imgui.PopStyleColor() 
imgui.PopStyleColor() 
imgui.PopStyleColor() 
imgui.PopStyleColor() 

imgui.PushStyleColor(imgui.constant.Col.Button, yellow) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_blue) --light blue
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, dark_green) --green
imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black

 imgui.SameLine()
imgui.SetCursorPosX(270)
 if imgui.Button("RESET WINCH") then
	reset_all()
 end
  imgui.TextUnformatted("")
 imgui.SetCursorPosX(60) 
 if imgui.Button("SPAWN ALL OBJECTS") then
	spawnallbuttonreloader = 1 
	command_once("x-winch/reload_winch_object")
	peopleSwitch = 1
	busSwitch = 1
	winchSwitch = 1
	chair1Switch = 1
	chair2Switch = 1
	operatorSwitch = 1
 end 
 
 imgui.SameLine()
imgui.SetCursorPosX(250)
 if imgui.Button("REMOVE ALL OBJECTS") then
	command_once("x-winch/kill_winch_object")
	peopleSwitch = 0
	busSwitch = 0
	winchSwitch = 0
	chair1Switch = 0
	chair2Switch = 0
	operatorSwitch = 0
 end 
 
  imgui.PopStyleColor()  
  imgui.PopStyleColor()   
  imgui.PopStyleColor()  
  imgui.PopStyleColor()  
  
  
  
  
  
-----------------------------------------------------------------
------------------------CHECK MARK BOX---------------------------  
-----------------------------------------------------------------
  
	imgui.TextUnformatted("")
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, white) -- check box background white
	imgui.PushStyleColor(imgui.constant.Col.CheckMark, dark_green) -- Check Mark Color Green
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, white) -- check box background white
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, white) -- check box background white
	imgui.SetCursorPosX(125)
        changed1, newVal1 = imgui.Checkbox("- Canopy Not Present/Closed", anything1 == true)
        if changed1 then
      if newVal1 == true then
			anything1 = true
      command_once("x-winch/canopy_not_present_canopy_close")
      elseif newVal1 == false then
			anything1 = false
			ftu_canopy_open = 1
      end
        end
imgui.PopStyleColor()     
imgui.PopStyleColor()
imgui.PopStyleColor()  
imgui.PopStyleColor()   
 
 
 
 
 
-----------------------------------------------------------------
------------------------WIND DIRECTION (DEPRICATED)-------------- 
-----------------------------------------------------------------
 
imgui.TextUnformatted("")
--[[imgui.SetCursorPosX(150)
imgui.TextUnformatted("Wind Direction: ")
imgui.SameLine()
imgui.SetCursorPosX(260)
imgui.TextUnformatted(Wind_WDir - (Wind_WDir % .1))
imgui.SetCursorPosX(150)
imgui.TextUnformatted("Wind Speed: ")
imgui.SameLine()
imgui.SetCursorPosX(240)
imgui.TextUnformatted(Wind_WSpd - (Wind_WSpd % .1))
 imgui.TextUnformatted("")]]
 
-----------------------------------------------------------------
------------------------SAVE PRESET 1----------------------------
----------------------------------------------------------------- 
 

 
 imgui.SetCursorPosX(110)
if imgui.BeginCombo("<- SAVE Current Scene To Preset", "", imgui.constant.ComboFlags.NoPreview) then
            
	if imgui.Selectable("Preset 1", choice == 1) then
	choice = 1
	
	local savefile2 = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset1.txt", "w")) -- open file as writable
	local savesuccess2 = false
	
	savefile2:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile2:write (bus_rotation..'\n')
	savefile2:write (busOffset..'\n')
	savefile2:write (people_rotation..'\n')
	savefile2:write (peopleOffset..'\n')
	savefile2:write (chair1_rotation..'\n')
	savefile2:write (chair1Offset..'\n')
	savefile2:write (chair2_rotation..'\n')
	savefile2:write (chair2Offset..'\n')
	savefile2:write (busSwitch..'\n')
	savefile2:write (peopleSwitch..'\n')
	savefile2:write (chair1Switch..'\n')
	savefile2:write (chair2Switch..'\n')
	
	
	
		if savefile2 then 
		savesuccess2 = true
		end
	
	savefile2:close() -- can't forget to close the file after writing  ]]
	end
	
	
-----------------------------------------------------------------
------------------------SAVE PRESET 2----------------------------
----------------------------------------------------------------- 
			
	if imgui.Selectable("Preset 2", choice == 2) then
	choice = 2
		
	local savefile3 = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset2.txt", "w")) -- open file as writable
	local savesuccess3 = false
	
	savefile3:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile3:write (bus_rotation..'\n')
	savefile3:write (busOffset..'\n')
	savefile3:write (people_rotation..'\n')
	savefile3:write (peopleOffset..'\n')
	savefile3:write (chair1_rotation..'\n')
	savefile3:write (chair1Offset..'\n')
	savefile3:write (chair2_rotation..'\n')
	savefile3:write (chair2Offset..'\n')
	savefile3:write (busSwitch..'\n')
	savefile3:write (peopleSwitch..'\n')
	savefile3:write (chair1Switch..'\n')
	savefile3:write (chair2Switch..'\n')
	
	
	
		if savefile3 then 
		savesuccess3 = true
		end
	
	savefile3:close() -- can't forget to close the file after writing 	

	end
			
-----------------------------------------------------------------
------------------------SAVE PRESET 3----------------------------
----------------------------------------------------------------- 			
			
	if imgui.Selectable("Preset 3", choice == 3) then
	choice = 3
				
	local savefile4 = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset3.txt", "w")) -- open file as writable
	local savesuccess4 = false
	
	savefile4:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile4:write (bus_rotation..'\n')
	savefile4:write (busOffset..'\n')
	savefile4:write (people_rotation..'\n')
	savefile4:write (peopleOffset..'\n')
	savefile4:write (chair1_rotation..'\n')
	savefile4:write (chair1Offset..'\n')
	savefile4:write (chair2_rotation..'\n')
	savefile4:write (chair2Offset..'\n')
	savefile4:write (busSwitch..'\n')
	savefile4:write (peopleSwitch..'\n')
	savefile4:write (chair1Switch..'\n')
	savefile4:write (chair2Switch..'\n')
	
	
	
		if savefile4 then 
		savesuccess4 = true
		end
	
	savefile4:close() -- can't forget to close the file after writing 		
				
	end
			
-----------------------------------------------------------------
------------------------SAVE PRESET 4----------------------------
----------------------------------------------------------------- 			
			
			
	if imgui.Selectable("Preset 4", choice == 4) then
	choice = 4
				
	local savefile5 = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset4.txt", "w")) -- open file as writable
	local savesuccess5 = false
	
	savefile5:write (winch_cable_length - (winch_cable_length % .1)..'\n') -- writes the first line, modulus trims off the float decimals, \n gives a new line
	savefile5:write (bus_rotation..'\n')
	savefile5:write (busOffset..'\n')
	savefile5:write (people_rotation..'\n')
	savefile5:write (peopleOffset..'\n')
	savefile5:write (chair1_rotation..'\n')
	savefile5:write (chair1Offset..'\n')
	savefile5:write (chair2_rotation..'\n')
	savefile5:write (chair2Offset..'\n')
	savefile5:write (busSwitch..'\n')
	savefile5:write (peopleSwitch..'\n')
	savefile5:write (chair1Switch..'\n')
	savefile5:write (chair2Switch..'\n')
	
	
	
		if savefile5 then 
		savesuccess5 = true
		end
	
	savefile5:close() -- can't forget to close the file after writing 	
				
            
	end 
	imgui.EndCombo()
end
	
		
		
		
imgui.PushStyleColor(imgui.constant.Col.Button, dark_green) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_green) -- green
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, red) --light blue 		
-----------------------------------------------------------------
------------------------LOAD PRESET 1----------------------------
----------------------------------------------------------------- 

		  imgui.TextUnformatted("")
 imgui.SetCursorPosX(180)
 imgui.TextUnformatted("LOAD SCENE:")
  imgui.SetCursorPosX(40)
 
 if imgui.Button(" PRESET 1 ") then
    p1buttonreloader = 1 
			local preset1file = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset1.txt", "r")) -- open file as readable
	
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			winch_cable_length = readline -- set winch cable length to the number read on the first line
			logMsg("Winch Length Line set to last used length")
			if winch_cable_length == 0 then
				winch_cable_length = 1200
			end
		else
				winch_cable_length = 1200 -- if first line is nil, set to 1200
				logMsg("Winch Length line did not read successfully, set to default")
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			bus_rotation = readline -- set winch cable length to the number read on the second line
			if bus_rotation == 0 then
				bus_rotation = 1.46
			end
		else
				bus_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busOffset = readline -- set winch cable length to the number read on the third line
			if busOffset == 0 then
				busOffset = 35
			end
		else
				busOffset = 35 -- if first line is nil, set to 1.46
		end
		
			readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			people_rotation = readline -- set winch cable length to the number read on the fourth line
			if people_rotation == 0 then
				people_rotation = 1.46
			end
		else
				people_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleOffset = readline -- set winch cable length to the number read on the fifth line
			if peopleOffset == 0 then
				peopleOffset = 30
			end
		else
				peopleOffset = 30 -- if first line is nil, set to 1.46
		end
		
					readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1_rotation = readline -- set winch cable length to the number read on the sixth line
			if chair1_rotation == 0 then
				chair1_rotation = 1.4
			end
		else
				chair1_rotation = 1.4 -- if first line is nil, set to 1.46
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Offset = readline -- set winch cable length to the number read on the seventh line
			if chair1Offset == 0 then
				chair1Offset = 30
			end
		else
				chair1Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2_rotation = readline -- set winch cable length to the number read on the eighth line
			if chair2_rotation == 0 then
				chair2_rotation = 1.35
			end
		else
				chair2_rotation = 1.35 -- if first line is nil, set to 1.46
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Offset = readline -- set winch cable length to the number read on the ninth line
			if chair2Offset == 0 then
				chair2Offset = 30
			end
		else
				chair2Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busSwitch = readline -- 
		else
				busSwitch = 1 -- 
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleSwitch = readline -- 
		else
				peopleSwitch = 1 -- 
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Switch = readline -- 
		else
				chair1Switch = 1 -- 
		end
		
		readline = preset1file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Switch = readline -- 
		else
				chair2Switch = 1 -- 
		end
		

		
		
	preset1file:close() -- can't forget to close after reading
command_once("x-winch/reload_winch_object")
 end
 
-----------------------------------------------------------------
------------------------LOAD PRESET 2----------------------------
-----------------------------------------------------------------  
 
 imgui.SameLine()
 imgui.SetCursorPosX(140) 
 
 
 if imgui.Button(" PRESET 2 ") then
	p2buttonreloader = 1 
				local preset2file = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset2.txt", "r")) -- open file as readable
	
	readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			winch_cable_length = readline -- set winch cable length to the number read on the first line
			logMsg("Winch Length Line set to last used length")
			if winch_cable_length == 0 then
				winch_cable_length = 1200
			end
		else
				winch_cable_length = 1200 -- if first line is nil, set to 1200
				logMsg("Winch Length line did not read successfully, set to default")
		end
		
	readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			bus_rotation = readline -- set winch cable length to the number read on the second line
			if bus_rotation == 0 then
				bus_rotation = 1.46
			end
		else
				bus_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busOffset = readline -- set winch cable length to the number read on the third line
			if busOffset == 0 then
				busOffset = 35
			end
		else
				busOffset = 35 -- if first line is nil, set to 1.46
		end
		
			readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			people_rotation = readline -- set winch cable length to the number read on the fourth line
			if people_rotation == 0 then
				people_rotation = 1.46
			end
		else
				people_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleOffset = readline -- set winch cable length to the number read on the fifth line
			if peopleOffset == 0 then
				peopleOffset = 30
			end
		else
				peopleOffset = 30 -- if first line is nil, set to 1.46
		end
		
					readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1_rotation = readline -- set winch cable length to the number read on the sixth line
			if chair1_rotation == 0 then
				chair1_rotation = 1.4
			end
		else
				chair1_rotation = 1.4 -- if first line is nil, set to 1.46
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Offset = readline -- set winch cable length to the number read on the seventh line
			if chair1Offset == 0 then
				chair1Offset = 30
			end
		else
				chair1Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2_rotation = readline -- set winch cable length to the number read on the eighth line
			if chair2_rotation == 0 then
				chair2_rotation = 1.35
			end
		else
				chair2_rotation = 1.35 -- if first line is nil, set to 1.46
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Offset = readline -- set winch cable length to the number read on the ninth line
			if chair2Offset == 0 then
				chair2Offset = 30
			end
		else
				chair2Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busSwitch = readline -- 
		else
				busSwitch = 1 -- 
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleSwitch = readline -- 
		else
				peopleSwitch = 1 -- 
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Switch = readline -- 
		else
				chair1Switch = 1 -- 
		end
		
		readline = preset2file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Switch = readline -- 
		else
				chair2Switch = 1 -- 
		end		
		
	preset2file:close() -- can't forget to close after reading	
	command_once("x-winch/reload_winch_object")

 end
 
-----------------------------------------------------------------
------------------------LOAD PRESET 3----------------------------
-----------------------------------------------------------------  
 
 imgui.SameLine()
 imgui.SetCursorPosX(240) 
 
 
 if imgui.Button(" PRESET 3 ") then
	p3buttonreloader = 1 
	local preset3file = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset3.txt", "r")) -- open file as readable
	
	readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			winch_cable_length = readline -- set winch cable length to the number read on the first line
			logMsg("Winch Length Line set to last used length")
			if winch_cable_length == 0 then
				winch_cable_length = 1200
			end
		else
				winch_cable_length = 1200 -- if first line is nil, set to 1200
				logMsg("Winch Length line did not read successfully, set to default")
		end
		
	readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			bus_rotation = readline -- set winch cable length to the number read on the second line
			if bus_rotation == 0 then
				bus_rotation = 1.46
			end
		else
				bus_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busOffset = readline -- set winch cable length to the number read on the third line
			if busOffset == 0 then
				busOffset = 35
			end
		else
				busOffset = 35 -- if first line is nil, set to 1.46
		end
		
			readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			people_rotation = readline -- set winch cable length to the number read on the fourth line
			if people_rotation == 0 then
				people_rotation = 1.46
			end
		else
				people_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleOffset = readline -- set winch cable length to the number read on the fifth line
			if peopleOffset == 0 then
				peopleOffset = 30
			end
		else
				peopleOffset = 30 -- if first line is nil, set to 1.46
		end
		
					readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1_rotation = readline -- set winch cable length to the number read on the sixth line
			if chair1_rotation == 0 then
				chair1_rotation = 1.4
			end
		else
				chair1_rotation = 1.4 -- if first line is nil, set to 1.46
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Offset = readline -- set winch cable length to the number read on the seventh line
			if chair1Offset == 0 then
				chair1Offset = 30
			end
		else
				chair1Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2_rotation = readline -- set winch cable length to the number read on the eighth line
			if chair2_rotation == 0 then
				chair2_rotation = 1.35
			end
		else
				chair2_rotation = 1.35 -- if first line is nil, set to 1.46
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Offset = readline -- set winch cable length to the number read on the ninth line
			if chair2Offset == 0 then
				chair2Offset = 30
			end
		else
				chair2Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busSwitch = readline -- 
		else
				busSwitch = 1 -- 
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleSwitch = readline -- 
		else
				peopleSwitch = 1 -- 
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Switch = readline -- 
		else
				chair1Switch = 1 -- 
		end
		
		readline = preset3file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Switch = readline -- 
		else
				chair2Switch = 1 -- 
		end				
		
	preset3file:close() -- can't forget to close after reading	
	command_once("x-winch/reload_winch_object")
 
 end
 
-----------------------------------------------------------------
------------------------LOAD PRESET 4----------------------------
-----------------------------------------------------------------  
 
 imgui.SameLine()
 imgui.SetCursorPosX(340) 
 
 p4buttonreloader = 0
 if imgui.Button(" PRESET 4 ") then
	p4buttonreloader = 1 

	
	local preset4file = (io.open(SCRIPT_DIRECTORY ..DIRECTORY_SEPARATOR.. "X-Winch/preset4.txt", "r")) -- open file as readable
	
	readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			winch_cable_length = readline -- set winch cable length to the number read on the first line
			logMsg("Winch Length Line set to last used length")
			if winch_cable_length == 0 then
				winch_cable_length = 1200
			end
		else
				winch_cable_length = 1200 -- if first line is nil, set to 1200
				logMsg("Winch Length line did not read successfully, set to default")
		end
		
	readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			bus_rotation = readline -- set winch cable length to the number read on the second line
			if bus_rotation == 0 then
				bus_rotation = 1.46
			end
		else
				bus_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busOffset = readline -- set winch cable length to the number read on the third line
			if busOffset == 0 then
				busOffset = 35
			end
		else
				busOffset = 35 -- if first line is nil, set to 1.46
		end
		
			readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			people_rotation = readline -- set winch cable length to the number read on the fourth line
			if people_rotation == 0 then
				people_rotation = 1.46
			end
		else
				people_rotation = 1.46 -- if first line is nil, set to 1.46
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleOffset = readline -- set winch cable length to the number read on the fifth line
			if peopleOffset == 0 then
				peopleOffset = 30
			end
		else
				peopleOffset = 30 -- if first line is nil, set to 1.46
		end
		
					readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1_rotation = readline -- set winch cable length to the number read on the sixth line
			if chair1_rotation == 0 then
				chair1_rotation = 1.4
			end
		else
				chair1_rotation = 1.4 -- if first line is nil, set to 1.46
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Offset = readline -- set winch cable length to the number read on the seventh line
			if chair1Offset == 0 then
				chair1Offset = 30
			end
		else
				chair1Offset = 30 -- if first line is nil, set to 1.46
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2_rotation = readline -- set winch cable length to the number read on the eighth line
			if chair2_rotation == 0 then
				chair2_rotation = 1.35
			end
		else
				chair2_rotation = 1.35 -- if first line is nil, set to 1.46
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Offset = readline -- set winch cable length to the number read on the ninth line
			if chair2Offset == 0 then
				chair2Offset = 30
			end
		else
				chair2Offset = 30 -- if first line is nil, set to 1.46
		end
		
				readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			busSwitch = readline -- 
		else
				busSwitch = 1 -- 
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			peopleSwitch = readline -- 
		else
				peopleSwitch = 1 -- 
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair1Switch = readline -- 
		else
				chair1Switch = 1 -- 
		end
		
		readline = preset4file:read("*line") -- read the first line in the file, store value as variable "readline"
		if readline ~= nil  then
			-- logInfo("On-screen notifications is: " .. readline)
			chair2Switch = readline -- 
		else
				chair2Switch = 1 -- 
		end		
		
		
	preset4file:close() -- can't forget to close after reading					

end
	
 --end
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()

    end



  
-----------------------------------------------------------------
----------------------BUS ROTATION SLIDER------------------------
-----------------------------------------------------------------


if bus_tab == 1 then
general_tab = 0
people_tab = 0
chair1_tab = 0
chair2_tab = 0 

imgui.TextUnformatted("")
imgui.TextUnformatted("")
imgui.TextUnformatted("")
	imgui.PushItemWidth(400)
	local cx2, cy2 = imgui.GetCursorScreenPos()
	imgui.SetCursorPosX(cx2 + 195)

	imgui.Image(glidericon, 60, 40)
	
	imgui.DrawList_AddCircle(cx2 + 225, cy2 + 16, 40, bold_green)
	
	
	imgui.DrawList_AddCircleFilled( (cx2 +225) - 40 * math.cos (-1.57 + (bus_rotation_adjusted / 100)), (cy2 +16) - 40 * math.sin (-1.57 + (bus_rotation_adjusted / 100)) , 5, red)
	
	imgui.PushItemWidth(445)
	
	imgui.TextUnformatted("")


	imgui.TextUnformatted("")
		imgui.SetCursorPosX(10)
	imgui.TextUnformatted("                          BUS ROTATION")



	imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
    bus_rotation_adjusted = bus_rotation * 100
	sliderVal2 = bus_rotation * 100
   local changed2, newVal2 = imgui.SliderFloat("Clockwise", sliderVal2, 0, 628, "Position Around Aircraft")
  
	if changed2 then
		sliderVal2 = newVal2
		bus_rotation_adjusted = newVal2 
		command_once("x-winch/reload_winch_object")
	
	end

   bus_rotation = bus_rotation_adjusted / 100
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 

-----------------------------------------------------------------
------------------------------BUS DISTANCE SLIDER----------------
-----------------------------------------------------------------

imgui.PushItemWidth(445)
	local sliderVal3 = busOffset
	imgui.TextUnformatted("")
	imgui.SetCursorPosX(10)
    imgui.TextUnformatted("                          BUS DISTANCE")
	
		imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
		imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
   local changed3, newVal3 = imgui.SliderFloat(" ", sliderVal3, 15, 60, "Meters: %.0f")
  
	if changed3 then
		sliderVal3 = newVal3
		busOffset = newVal3 
	command_once("x-winch/reload_winch_object")
    end
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 

-----------------------------------------------------------------
-----------------------------BUS APPLY CHANGES-------------------
-----------------------------------------------------------------

--imgui.TextUnformatted("")
imgui.PushStyleColor(imgui.constant.Col.Button, dark_green) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_green) -- green
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, red) --light blue
imgui.TextUnformatted("")
imgui.SetCursorPosX(190)
if imgui.Button(" SPAWN BUS ") then
	busreloader = 1 
	busSwitch = 1 
	command_once("x-winch/reload_winch_object")
end
 
 
-----------------------------------------------------------------
-----------------------------------REMOVE BUS--------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.SetCursorPosX(188)
 if imgui.Button(" REMOVE BUS ") then
	busSwitch = 0
	command_once("x-winch/kill_bus")	
 end  
imgui.TextUnformatted("")
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 
    end







-----------------------------------------------------------------
--------------------PEOPLE ROTATION SLIDER-----------------------
-----------------------------------------------------------------

if people_tab == 1 then
bus_tab = 0
general_tab = 0
chair1_tab = 0
chair2_tab = 0 

		local cx3, cy3 = imgui.GetCursorScreenPos()
		imgui.TextUnformatted("")
imgui.TextUnformatted("")
imgui.TextUnformatted("")
	imgui.PushItemWidth(445)
	imgui.SetCursorPosX(cx3 + 195)

	imgui.Image(glidericon, 60, 40)
	
	imgui.DrawList_AddCircle(cx3 + 225, cy3 +67 , 40, bold_green)
	
	
	imgui.DrawList_AddCircleFilled( (cx3 +225) - 40 * math.cos (-1.57 + (people_rotation_adjusted / 100)), (cy3 +67) - 40 * math.sin (-1.57 + (people_rotation_adjusted / 100)) , 5, red)
	
	imgui.PushItemWidth(445)
	people_rotation_adjusted = people_rotation * 100
	local sliderVal4 = people_rotation_adjusted
	imgui.TextUnformatted("")


	imgui.TextUnformatted("")
	imgui.TextUnformatted("                         PEOPLE ROTATION")
	
	imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
	
   local changed4, newVal4 = imgui.SliderFloat("Rotation", sliderVal4, 0, 628, "Position Around Aircraft")
  
	if changed4 then
		sliderVal4 = newVal4
		people_rotation_adjusted = newVal4 
	command_once("x-winch/reload_winch_object")
	end

    people_rotation = people_rotation_adjusted / 100
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 
 
----------------------------------------------------------------- 
--------------PEOPLE DISTANCE SLIDER-----------------------------
-----------------------------------------------------------------

imgui.PushItemWidth(445)
	local sliderVal5 = peopleOffset
	imgui.TextUnformatted("")
    imgui.TextUnformatted("                         PEOPLE DISTANCE")
	
		imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
		imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
	
   local changed5, newVal5 = imgui.SliderFloat("  ", sliderVal5, 15, 60, "Meters: %.0f")
  
	if changed5 then
		sliderVal5 = newVal5
		peopleOffset = newVal5
	command_once("x-winch/reload_winch_object")
end
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 

-----------------------------------------------------------------
-------------PEOPLE APPLY CHANGES--------------------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.PushStyleColor(imgui.constant.Col.Button, dark_green) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_green) -- green
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, red) --light blue
imgui.SetCursorPosX(180)
 if imgui.Button(" SPAWN PEOPLE ") then
	peoplereloader = 1  
	peopleSwitch = 1 
	command_once("x-winch/reload_winch_object")
 end 	
 
----------------------------------------------------------------- 
-------------REMOVE PEOPLE---------------------------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.SetCursorPosX(178)
 if imgui.Button(" REMOVE PEOPLE ") then
	peopleSwitch = 0 
	command_once("x-winch/kill_people")
 end 
	imgui.TextUnformatted("") 
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 
    end








-----------------------------------------------------------------
--------------CHAIR 1 ROTATION SLIDER----------------------------
-----------------------------------------------------------------

imgui.PushItemWidth(400)
if chair1_tab == 1 then
	bus_tab = 0
people_tab = 0
general_tab = 0
chair2_tab = 0 
	local cx4, cy4 = imgui.GetCursorScreenPos()
			imgui.TextUnformatted("")
imgui.TextUnformatted("")
imgui.TextUnformatted("")
	imgui.PushItemWidth(445)
	imgui.SetCursorPosX(cx4 + 195)

	imgui.Image(glidericon, 60, 40)
	
	imgui.DrawList_AddCircle(cx4 + 225, cy4 + 67, 40, bold_green)
	
	
	imgui.DrawList_AddCircleFilled( (cx4 +225) - 40 * math.cos (-1.57 + (chair1_rotation_adjusted / 100)), (cy4 +67) - 40 * math.sin (-1.57 + (chair1_rotation_adjusted / 100)) , 5, red)
	
	imgui.PushItemWidth(445)
	chair1_rotation_adjusted = chair1_rotation * 100
	local sliderVal6 = chair1_rotation * 100
	imgui.TextUnformatted("")


	imgui.TextUnformatted("")
	imgui.TextUnformatted("                        CHAIR 1 ROTATION")
	
	imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
	
   local changed6, newVal6 = imgui.SliderFloat(" Clockwise", sliderVal6, 0, 628, "Position Around Aircraft")
  
	if changed6 then
		sliderVal6 = newVal6
		chair1_rotation_adjusted = newVal6 
	command_once("x-winch/reload_winch_object")
	end

    chair1_rotation = chair1_rotation_adjusted / 100

imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 

-----------------------------------------------------------------
--------------CHAIR 1 DISTANCE SLIDER----------------------------
-----------------------------------------------------------------

imgui.PushItemWidth(445)

	local sliderVal7 = chair1Offset
	imgui.TextUnformatted("")
    imgui.TextUnformatted("                        CHAIR 1 DISTANCE")
	
		imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
		imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
 	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black  
   local changed7, newVal7 = imgui.SliderFloat("   ", sliderVal7, 15, 60, "Meters: %.0f")
  
	if changed7 then
		sliderVal7 = newVal7
		chair1Offset = newVal7
	command_once("x-winch/reload_winch_object")
end
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 

-----------------------------------------------------------------
-------------CHAIR 1 APPLY CHANGES-------------------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.PushStyleColor(imgui.constant.Col.Button, dark_green) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_green) -- green
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, red) --light blue
imgui.SetCursorPosX(180)
 if imgui.Button(" SPAWN CHAIR ") then
	chair1reloader = 1  
	chair1Switch = 1 
	command_once("x-winch/reload_winch_object")
 end 
 
----------------------------------------------------------------- 
-------------REMOVE CHAIR 1--------------------------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.SetCursorPosX(173)
 if imgui.Button(" REMOVE CHAIR 1 ") then
	chair1Switch = 0 
	command_once("x-winch/kill_chair1")
 end 
	imgui.TextUnformatted("") 
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 
    end









-----------------------------------------------------------------
--------------CHAIR 2 ROTATION SLIDER----------------------------
-----------------------------------------------------------------

imgui.PushItemWidth(445)
if chair2_tab == 1 then
	bus_tab = 0
people_tab = 0
chair1_tab = 0
general_tab = 0 
	local cx5, cy5 = imgui.GetCursorScreenPos()
			imgui.TextUnformatted("")
imgui.TextUnformatted("")
imgui.TextUnformatted("")
	imgui.PushItemWidth(445)
	imgui.SetCursorPosX(cx5 + 195)

	imgui.Image(glidericon, 60, 40)
	
	imgui.DrawList_AddCircle(cx5 + 225, cy5 + 67, 40, bold_green)
	
	
	imgui.DrawList_AddCircleFilled( (cx5 +225) - 40 * math.cos (-1.57 + (chair2_rotation_adjusted / 100)), (cy5 +67) - 40 * math.sin (-1.57 + (chair2_rotation_adjusted / 100)) , 5, red)
	
	imgui.PushItemWidth(445)
	chair2_rotation_adjusted = chair2_rotation * 100
	local sliderVal8 = chair2_rotation * 100
	imgui.TextUnformatted("")


	imgui.TextUnformatted("")
	imgui.TextUnformatted("                        CHAIR 2 ROTATION")
	
	imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black 
	
   local changed8, newVal8 = imgui.SliderFloat(" Rotation", sliderVal8, 0, 628, "Position Around Aircraft")
	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
	
	if changed8 then
		sliderVal8 = newVal8
		chair2_rotation_adjusted = newVal8 
	command_once("x-winch/reload_winch_object")
	end

    chair2_rotation = chair2_rotation_adjusted / 100
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
  imgui.PopStyleColor() 

-----------------------------------------------------------------
--------------CHAIR 2 DISTANCE SLIDER----------------------------
-----------------------------------------------------------------

imgui.PushItemWidth(445)
	local sliderVal9 = chair2Offset
	imgui.TextUnformatted("")
    imgui.TextUnformatted("                        CHAIR 2 DISTANCE")
	imgui.PushStyleColor(imgui.constant.Col.SliderGrab, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.SliderGrabActive, red) -- slider grab tab red
	imgui.PushStyleColor(imgui.constant.Col.FrameBg, light_blue) -- slider background light blue
	imgui.PushStyleColor(imgui.constant.Col.FrameBgHovered, yellow) -- slider background hovered yellow
	imgui.PushStyleColor(imgui.constant.Col.FrameBgActive, yellow) -- slider background hovered yellow
 	imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black  
   local changed9, newVal9 = imgui.SliderFloat("    ", sliderVal9, 15, 60, "Meters: %.0f")
  
	if changed9 then
		sliderVal9 = newVal9
		chair2Offset = newVal9
		command_once("x-winch/reload_winch_object")
	end
	
imgui.PopItemWidth()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()

-----------------------------------------------------------------
-------------CHAIR 2 APPLY CHANGES-------------------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.PushStyleColor(imgui.constant.Col.Button, dark_green) --yellow
imgui.PushStyleColor(imgui.constant.Col.ButtonHovered, light_green) -- green
imgui.PushStyleColor(imgui.constant.Col.ButtonActive, red) --light blue
--imgui.PushStyleColor(imgui.constant.Col.Text, black) -- Black
imgui.SetCursorPosX(180)
 if imgui.Button(" SPAWN CHAIR ") then
	chair2reloader = 1 
	chair2Switch = 1 
	command_once("x-winch/reload_winch_object")
 end 	
 
----------------------------------------------------------------- 
-------------REMOVE CHAIR 2--------------------------------------
-----------------------------------------------------------------

imgui.TextUnformatted("")
imgui.SetCursorPosX(173)
 if imgui.Button(" REMOVE CHAIR 2 ") then
	chair2Switch = 0 
	command_once("x-winch/kill_chair2")
 end 
	imgui.TextUnformatted("") 
imgui.PopStyleColor()
imgui.PopStyleColor()
imgui.PopStyleColor()

end -- chair 2 tab section





end  --winchcp_on_build




-----------------------------------------------------------------
-----------SHOW/HIDE WINDOW BUILDER---------------
-----------------------------------------------------------------

function winchcp_show_wnd()
    winchcp_wnd = float_wnd_create(460, 460, 1, true) -- Window Size
	float_wnd_set_title(winchcp_wnd, "X-Winch Control Panel v. 1.0.3")
    float_wnd_set_imgui_builder(winchcp_wnd, "winchcp_on_build")
end

function winchcp_hide_wnd()
    if winchcp_wnd then
        float_wnd_destroy(winchcp_wnd)
    end
end





-----------------------------------------------------------------
------------AUTO HIDE ON HOOKUP WINDOW LOGIC------------
-----------------------------------------------------------------

function force_hide_wnd() -- kills the pop up on "hookup" and keeps it hidden until flight restart on the winch.
	if hook_up_sound_flag == 1 then
		winchcp_hide_wnd()
	end
end




-----------------------------------------------------------------
-----------STANDARD SHOW/HIDE WINDOW LOGIC------------
-----------------------------------------------------------------

winchcp_show_only_once = 0
winchcp_hide_only_once = 0

function toggle_winch_control_panel()
	winchcp_show_window = not winchcp_show_window
	if winchcp_show_window then
		if winchcp_show_only_once == 0 then
			winchcp_show_wnd()
			winchcp_show_only_once = 1
			winchcp_hide_only_once = 0
		end
	else
		if winchcp_hide_only_once == 0 then
			winchcp_hide_wnd()
			winchcp_hide_only_once = 1
			winchcp_show_only_once = 0
		end
	end
end


-----------------------------------------------------------------
-----------AUTOMATIC BUTTON DOUBLE CLICK FOR RELOAD--------------
-----------------------------------------------------------------



function buttonreloader()

if p4buttonreloader == 1 then
	command_once("x-winch/reload_winch_object")
p4buttonreloader = 0
end

if p3buttonreloader == 1 then
	command_once("x-winch/reload_winch_object")
p3buttonreloader = 0
end

if p2buttonreloader == 1 then
	command_once("x-winch/reload_winch_object")
p2buttonreloader = 0
end

if p1buttonreloader == 1 then
	command_once("x-winch/reload_winch_object")
p1buttonreloader = 0
end

if spawnallbuttonreloader == 1 then
	command_once("x-winch/reload_winch_object")
spawnallbuttonreloader = 0
end

if busreloader == 1 then
	command_once("x-winch/reload_winch_object")
busreloader = 0
end

if peoplereloader == 1 then
	command_once("x-winch/reload_winch_object")
peoplereloader = 0
end

if chair1reloader == 1 then
	command_once("x-winch/reload_winch_object")
chair1reloader = 0
end

if chair2reloader == 1 then
	command_once("x-winch/reload_winch_object")
chair2reloader = 0
end


end


-----------------------------------------------------------------
-----------MACRO ADDS, COMMAND CREATION, FUNCTION CALLS------------
-----------------------------------------------------------------


add_macro("Winch Contol Panel: open/close", "winchcp_show_wnd()", "winchcp_hide_wnd()", "deactivate")
create_command("x-winch/initiate_launch", "This command will initiate a winch launch Hook Up", "initiate_winch_launch()", "", "")
create_command("x-winch/winch_control_panel_show_toggle", "open/close X-Winch Control Panel", "toggle_winch_control_panel()", "", "")
create_command("x-winch/canopy_not_present_canopy_close", "Canopy Not Present / Canopy Closed", "canopy_not_present()", "", "")
do_every_frame("envirosounds()")
do_every_frame("canopy_open_start()")
do_every_frame("start_launch_sequence()")
do_every_frame("force_hide_wnd()")
do_every_frame("writewinchlength()")
do_every_frame("readwinchlength()")
do_every_frame("startdelay()")
do_every_frame("buttonreloader()")

